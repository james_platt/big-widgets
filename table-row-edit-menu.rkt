#lang racket

(require racket/gui)
(provide  attach-row-edit-menu-items)


(define (attach-row-edit-menu-items m qresults-table)
  #;(-> (is-a?/c popup-menu<%>)
       any)


;Keyboard shortcuts defined in menu-items which only appear in a popup menu will only work when
;the popup menu is shown.  This is not an issue if you follow Apple's HIG because any item in
;a popup menu is also supposed to appear in the main menu bar, in a contextual way.  Then the
;keyboard shortcuts will be available all the time.  

(define info-menu-item (new menu-item%
                              (label "info")
                              (parent m)
                              (shortcut #\i)
                              ;(shortcut-prefix 'cmd)
                              ;(callback (lambda (x y) x))
                              (callback (lambda (menu-item event)
                                          ;(define row-index (send qresults-table get-selected-row-index))
                                          (message-box "Info"
                                                       (~a "You have selected " (length (send qresults-table get-selected-row-indexes)) " rows")
                                                       #f)
                                          ))
                              ))

(define delete-menu-item  (new menu-item%
                               (label "Delete Row(s)")
                               (parent m)
                               (shortcut #\backspace); command-delete on Mac, control-backspace on Linux and Windows.
                               ;(shortcut-prefix 'cmd)
                               ;(callback (lambda (x y) x))
                               (callback (lambda (menu-item event)
                                           ;(message-box "selections " (~a (send qresults-table get-selected-row-indexes)) #f)
                                           (for ([row (reverse (send qresults-table get-selected-row-indexes))])
                                             ;Need to delete in reverse order because the rows renumber after each iteration of the loop.
                                             ;(displayln (~a "deleting row " row))
                                             (send qresults-table delete-row row))
                                           ))
                               ))

(define insert-before (new menu-item%
                               (label "Insert before selection")
                               (parent m)
                               (shortcut #\b)
                               (callback (lambda (menu-item event)
                                           ;(displayln (send qresults-table get-selected-row-index))
                                           (displayln (format  "row count is ~a n"
                                                               (send qresults-table get-row-count)))
                                           #;
                                           (displayln (format "here is a blank row ~a ."
                                                              (build-list (send qresults-table get-row-count)
                                                                          (lambda (r) ""))))
                                           
                                           (send qresults-table add-rows-at
                                                 (vector)
                                                 #;
                                                 (build-list (send qresults-table get-row-count)
                                                             (lambda (r) ""))
                                                 #:row-index (- (send qresults-table get-selected-row-index) 1))

                                           ;add a loop to add multiple rows at once
                                           #;
                                           (for ([i #;(send qresults-table get-selected-row-count)
                                                    (send qresults-table get-selected-row-count)
                                                    ])
                                             (send qresults-table add-rows-at
                                                   (build-vector (send qresults-table get-row-count)
                                                                 (lambda (r) ""))
                                                   #:row-index (send qresults-table get-selected-row-index))
                                             )
                                           ))
                               ))

(define add-new-row (new menu-item%
                         (label "New row")
                         (parent m)
                         (shortcut #\n)
                         (callback (lambda (menu-item event)
                                     (send qresults-table add-row (vector))
                                     ))
                         ))


(define edit-row (new menu-item%
                      (label "Edit")
                      (parent m)
                      (shortcut #\e)
                      (callback (lambda (menu-item event)
                                  (displayln (format "row data is ~a"
                                                     (send qresults-table get-data (send qresults-table get-selected-row-index))))
                                  ))))


(define (enable-disable-row-menu-items m qresults-table) ;for actions on rows of the table.
;Some menus need to be greyed out when there is no selection.  Otherwise, you get an error
  ;when you right click and hit an action which needs a selection.
  ;Each menu item which acts on one or more rows of the table should appear here in this function.
  (send info-menu-item enable (send qresults-table get-selected-row-index))
  (send delete-menu-item enable (send qresults-table get-selected-row-index))
  (send insert-before enable (send qresults-table get-selected-row-index ))
  )
  
"Row edit menu attached."
)

