#lang racket

(provide (all-defined-out))
;Functions for rearranging and transforming data.

;Transpose a list of lists similar to the way that t() does in R.
;Consider making this accomodate other data types for input. For
;example, a list of vectors.
;  lol = list of lists
;  il = inner list
(define (transpose_lol lol)
  (for*/list ([i (length (list-ref lol 0))])
    (for/list ((il lol))
      (list-ref il i))))

(define transpose-lol transpose_lol) ; alias in order to standardize _ vs -

; Sort a list of lists by a column specified by index number.
;Got this from Stack Overflow and slightly modified it.
; https://stackoverflow.com/questions/40050677/is-there-a-way-to-sort-a-list-of-lists-using-an-index
(define (sort-lol lol column-i)  ;;add comparitor as a parameter and use below.
  (sort lol
        (lambda(a b)
          (if (string<?  (list-ref a column-i)  (list-ref b column-i))
              #t #f))))

;This function is from StackOverflow
;https://stackoverflow.com/questions/16630702/what-racket-function-can-i-use-to-insert-a-value-into-an-arbitrary-position-with 
(define (insert-at lst pos x)
  (define-values (before after) (split-at lst pos))
  (append before (cons x after)))