#lang racket

(require racket/gui
         framework
         handy/utils
         "data_manipulation.rkt")

(provide (all-defined-out))


;--------------Sort a list-box table by a column.---------------------------------
;Toggle the sort to descending and back when a column is clicked repeatedly.
;State of the sort is stored in the data for the first row of the table.
;Data (as referenced by the get-data and set-data messages) is not displayed
;in a list-box table.  The items displayed in cells are actually the "choices"
;of the list box.  Sorting is performed on the underlying information used to create
;the table, which you pass to the table-data-lol parameter. Then it is redisplayed.
;If editing of the table is possible, then you will have to make sure that changes.
;are synced to the proper variable before this function is called.
;Values in the table are a list of lists of strings.  Therefore, everything is
;sorted as strings and not numbers or dates, for example.  
(define (sort-list-box-by-column
         lstbox ; List-box object.
         column-number ;to sort by, indexed from 0.
         table-data-lol
         )
  ;(say "column " (list-ref (send lstbox get-column-labels) column-number) " clicked")
  ;(say table-data-lol)
  ;(say (transpose-lol table-data-lol))
  (say "Sorting by column index " column-number ".")
  
  ;(say (transpose-lol (sort (transpose-lol table-data-lol) string<? #:key (curryr list-ref column-number))))

  (say "Previous state of sorting is: " (send lstbox get-data 0))

  (cond [(equal? (send lstbox get-data 0) (~a "column " column-number ", ascending"))
         (say "sort by " "column " column-number ", descending")
         (send/apply lstbox set
                     (transpose-lol (sort (transpose-lol table-data-lol) string>?
                                          #:key (curryr list-ref  column-number))))
         (send lstbox set-data 0 (~a "column " column-number ", descending"))]
        [else
         (say "sort by " "column " column-number ", ascending")
         (send/apply lstbox set
                     (transpose-lol (sort (transpose-lol table-data-lol) string<?
                                          #:key (curryr list-ref  column-number))))
         (send lstbox set-data 0 (~a "column " column-number ", ascending"))]
        )
  )


(define (report-row-single-click
         lstbox ; List-box object.
         row-number
         table-data-lol
         )
  (message-box "single click" (~a "Single clicked on " row-number ".") #f)
  )


(define (report-row-double-click
         lstbox ; List-box object.
         row-number
         table-data-lol
         )
  (message-box "Double click" (~a "Double clicked on " row-number ".") #f)
  )
