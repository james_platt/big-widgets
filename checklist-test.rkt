#lang racket

(require racket/gui
         "main.rkt"
         "menu-actions.rkt")



(define frame (new frame%
                   [label "Checklist"]
                   ;[width 200]
                   ;[height 100]
                   ))

;(make-menus frame)

(define checklist-items (list (hash 'item1 #f)
                              (hash 'item2 #t)
                              (hash 'item3 #f)
                              (hash 'item4 #t)
                              (hash 'item5 #f)
                              ))

(define my-control-font (make-font #:size 18))


(define my-checklist
  (make-checklist frame checklist-items
                  ;#:columns 2
                  #:font my-control-font
                  ))

(new button%
     [parent frame]
     [label "Show Values"]
     [callback (lambda (c e) (println (get-checklist-data my-checklist)))])



(send frame show #t)
